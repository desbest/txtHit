# txtHit

Created by [txtbox](https://archive.is/lRKVm) who now runs [cbox](http://cbox.ws)
Maintained by [desbest](http://desbest.com)

txtHit is a simple hit counter that stores all its data 
in a plain text file. Please read this file for 
installation instructions and help.

In this file:
 + Installation
 + Usage
 + Changelog


## [ INSTALLATION ]

Extract counter.txt, hitreport.php and txthit.php, and 
upload them to your site. CHMOD counter.txt to 777, so 
it is writable. You can normally do this from your 
FTP program (in WS_FTP, right click the file and select 
'chmod' from the context menu).

Then paste the following line of code into the PHP files 
you want to count hits on:

<?require('txthit.php');$hits = hit($_SERVER['PHP_SELF']);?>

Make sure you paste the line right at the beginning of 
your files, before any output occurs, or there 
may be errors.

To display the hit count for the current page, paste 
this wherever you want the count to appear:

<?=$hits?>


## [ USAGE ]

Open txthit.php in any text editor to change the few 
settings available. 

Browse to the hitreport.php page on your site to see a 
basic hit count report.

To hard-code the name of a page, replace the 
$_SERVER['PHP_SELF'] in the above code with a string, 
e.g. 'My Page'.

To reset or change hit counts, download the counter.txt 
file via FTP, modify it in a text editor, and re-upload. 
Stick to the format or the script will stop working.

If you enable unique-only counting, the code line must 
be the first line in the page, or the cookie necessary 
for the feature can't be set.

The most conventient setup is to have the hitcounter 
code in a header that is included by all content pages - 
that way you don't have to do anything to start counting 
on a new page.


## [ CHANGELOG ]

 * v1.0 [04-04-27]
   - First release. Based on old texthitcounter script.