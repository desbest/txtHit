<?
    /******************************
    *       txtHit v1.0           *
    *   �2004 - Thomas Love       *
    *   http://txtbox.co.za       *
    *   http://github.com/desbest *
    ***************************** */

  // The file to store counter data in. Must be writable (set to 777 with CHMOD).
$thcfg['counterfile'] = 'counter.txt';

  // If set to true, session cookies are used to prevent more than one hit 
  // from being recorded per person. In this case the include must be at the top of 
  // the page being counted.
$thcfg['onlyunique'] = false;

  // The name of the session cookie. If you are counting more than one page, each with an 
  // independent unique counter, each page must have a unique cookie name.
$thcfg['cookie'] = 'txthit1';

  // IP addresses to ignore. Useful if your IP is static, to ignore your own hits. 
  // IPs in standard form. Separate each address with a comma. 
$thcfg['ipignore'] = '';


// ---------------------


function hit ($pagename) {
  global $thcfg;

  $fp = @fopen($thcfg['counterfile'],"r+");
  if (!isset($fp)) die ('<font color="red">Error: could not open counter file.</font>');

  // read in the page values and look out for current page
  $cnt = 0;$hitcount = 0;$found = false;
  while ($line = fgets($fp, 4096)) {
    $data = explode(':',$line);
    if ($data[0] == $pagename) {
      $found = true;
      $hitcount = (int)$data[1]+1;
      $cnt += strlen($data[0])+1;
      break;
    }
    $cnt += strlen($line);
  }
  
  if (isset($thcfg['ipignore'])) {
    $ipignore = explode(',', $thcfg['ipignore']);
    foreach($ipignore as $ip) {
      if ($_SERVER['REMOTE_ADDR'] == trim($ip)) {
        $badip = true;
        break;
      }
    }
  }
  
  // only increment if not ignored ip
  if (!$badip) {
    // only increment if unique
    if (!$thcfg['onlyunique'] || !isset($_COOKIE[$thcfg['cookie']])) {
      // page was not found, so add it
      if (!$found) {
        $cnt = '0000000000000001';
        fwrite($fp,((filesize($thcfg['counterfile']) == 0)?'':"\r\n").$pagename.':'.$cnt.':'.time());
        $hitcount = (int)$cnt;
      }
      // page was found, so update its count
      else {
        fseek($fp,$cnt);
        fwrite($fp,str_pad($hitcount, 16, '0', STR_PAD_LEFT));
      }
      if ($thcfg['onlyunique'] && !isset($_COOKIE[$thcfg['cookie']])) setcookie($thcfg['cookie'],'1');
    }
  }

  return $hitcount;
}

?>