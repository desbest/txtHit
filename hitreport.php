<html><head><title>txtHit Report</title>
<style type="text/css">
body {
  font-family: Arial;
  font-size: 10pt;
  color: #000000;
}
td {
  font-family: Arial;
  font-size: 10pt;
  color: #000000;
}
</style>
</head>
<body>
<h3>txtHit Report</h3>
<?
require('txthit.php');

$fp = @fopen($thcfg['counterfile'],"r");
if (!isset($fp)) die ('<font color="red">Error: could not open counter file.</font>');

?><table border="0" cellpadding="3" cellspacing="1" bgcolor="#cdcdcd" style="border:#cdcdcd 3px double;"><tr>
<td bgcolor="#cdcdcd" align="center"><b>page</b></td>
<td bgcolor="#cdcdcd" align="center"><b>hits</b></td>
<td bgcolor="#cdcdcd" align="center"><b>counting since</b></td>
<td bgcolor="#cdcdcd" align="center"><b>avg. per day</b></td>
</tr><?

while ($line = fgets($fp)) {
  $got = true;
  $cnt = ($cnt)?false:true;
  $data = explode(':',$line);
  if (time()-$data[2] > 1) $perday = ceil($data[1]/((time()-$data[2])/86400));
  else $perday = 'n/a';
    
?><tr>
<td bgcolor="<?=($cnt)?'#efefef':'#ffffff'?>"><?=$data[0]?></td>
<td bgcolor="<?=($cnt)?'#efefef':'#ffffff'?>"><?=(int)$data[1]?></td>
<td bgcolor="<?=($cnt)?'#efefef':'#ffffff'?>"><?=date('d M Y H:i:s',$data[2])?> (<?=round((time()-$data[2])/86400,1)?> days)</td>
<td bgcolor="<?=($cnt)?'#efefef':'#ffffff'?>"><?=$perday?></td>
</tr><?
}
if (!$got) {
?><td bgcolor="#efefef" align="center" colspan="4"><i>No hits recorded yet</i></td><?
}
?>

</table>
<br><br><br><br>
<small>powered by 
<a href="http://github.com/desbest">desbest on Github</a></small>
</body>
</html>